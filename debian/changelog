php-horde-smtp (1.9.6-2) unstable; urgency=medium

  * d/patches: Update 1010_phpunit-8.x+9.x.patch. Add phpunit.xml file to tests
    subdirectory.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 05 Dec 2020 15:35:21 +0100

php-horde-smtp (1.9.6-1) unstable; urgency=medium

  * New upstream version 1.9.6

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 23 Nov 2020 12:54:20 +0100

php-horde-smtp (1.9.5-7) unstable; urgency=medium

  * d/patches: Add 1010_phpunit-8.x+9.x.patch. Fix tests with PHPUnit 8.x/9.x.
  * d/t/control: Require php-horde-test (>= 2.6.4+debian0-5~).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 14 Sep 2020 21:28:48 +0200

php-horde-smtp (1.9.5-6) unstable; urgency=medium

  [ Mike Gabriel ]
  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.
  * d/salsa-ci.yml: Add file with salsa-ci.yml and pipeline-jobs.yml calls.

  [ Juri Grabowski ]
  * d/salsa-ci.yml: enable aptly
  * d/salsa-ci.yml: allow_failure on autopkgtest.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 10:48:07 +0200

php-horde-smtp (1.9.5-5) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959307).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/upstream/metadata: Add file. Comply with DEP-12.
  * d/tests/control: Stop using deprecated needs-recommends restriction.
  * d/copyright: Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 07 May 2020 10:29:36 +0200

php-horde-smtp (1.9.5-4) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 20:13:10 +0200

php-horde-smtp (1.9.5-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 11:38:52 +0200

php-horde-smtp (1.9.5-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Thu, 05 Apr 2018 21:33:45 +0200

php-horde-smtp (1.9.5-1) unstable; urgency=medium

  * New upstream version 1.9.5

 -- Mathieu Parent <sathieu@debian.org>  Sat, 01 Jul 2017 21:51:07 +0200

php-horde-smtp (1.9.4-1) unstable; urgency=medium

  * New upstream version 1.9.4

 -- Mathieu Parent <sathieu@debian.org>  Tue, 24 Jan 2017 21:23:08 +0100

php-horde-smtp (1.9.3-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Wed, 08 Jun 2016 08:28:37 +0200

php-horde-smtp (1.9.3-1) unstable; urgency=medium

  * New upstream version 1.9.3

 -- Mathieu Parent <sathieu@debian.org>  Sat, 26 Mar 2016 11:59:59 +0100

php-horde-smtp (1.9.2-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Sun, 13 Mar 2016 16:43:40 +0100

php-horde-smtp (1.9.2-1) unstable; urgency=medium

  * New upstream version 1.9.2

 -- Mathieu Parent <sathieu@debian.org>  Wed, 03 Feb 2016 23:13:39 +0100

php-horde-smtp (1.9.1-3) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Fri, 23 Oct 2015 07:26:24 +0200

php-horde-smtp (1.9.1-2) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf

 -- Mathieu Parent <sathieu@debian.org>  Mon, 10 Aug 2015 00:52:11 +0200

php-horde-smtp (1.9.1-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 1.9.1

 -- Mathieu Parent <sathieu@debian.org>  Mon, 04 May 2015 22:10:27 +0200

php-horde-smtp (1.6.0-4) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 14:31:05 +0200

php-horde-smtp (1.6.0-3) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 15:04:52 +0200

php-horde-smtp (1.6.0-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 22:57:32 +0200

php-horde-smtp (1.6.0-1) unstable; urgency=medium

  * New upstream version 1.6.0

 -- Mathieu Parent <sathieu@debian.org>  Tue, 05 Aug 2014 11:20:33 +0200

php-horde-smtp (1.5.2-1) unstable; urgency=medium

  * New upstream version 1.5.2

 -- Mathieu Parent <sathieu@debian.org>  Thu, 19 Jun 2014 22:55:34 +0200

php-horde-smtp (1.5.1-1) unstable; urgency=medium

  * New upstream version 1.5.1

 -- Mathieu Parent <sathieu@debian.org>  Sun, 15 Jun 2014 21:33:12 +0200

php-horde-smtp (1.5.0-1) unstable; urgency=medium

  * New upstream version 1.5.0

 -- Mathieu Parent <sathieu@debian.org>  Wed, 04 Jun 2014 23:10:56 +0200

php-horde-smtp (1.4.1-1) unstable; urgency=medium

  * New upstream version 1.4.1

 -- Mathieu Parent <sathieu@debian.org>  Sun, 13 Apr 2014 10:28:26 +0200

php-horde-smtp (1.4.0-1) unstable; urgency=medium

  * New upstream version 1.4.0

 -- Mathieu Parent <sathieu@debian.org>  Sat, 15 Feb 2014 13:41:15 +0100

php-horde-smtp (1.3.1-1) unstable; urgency=low

  * New upstream version 1.3.1

 -- Mathieu Parent <sathieu@debian.org>  Sat, 30 Nov 2013 19:51:04 +0100

php-horde-smtp (1.3.0-1) unstable; urgency=low

  * New upstream version 1.3.0

 -- Mathieu Parent <sathieu@debian.org>  Sat, 02 Nov 2013 16:59:58 +0100

php-horde-smtp (1.2.6-1) unstable; urgency=low

  * New upstream version 1.2.6

 -- Mathieu Parent <sathieu@debian.org>  Tue, 29 Oct 2013 19:14:04 +0100

php-horde-smtp (1.2.5-1) unstable; urgency=low

  * New upstream version 1.2.5

 -- Mathieu Parent <sathieu@debian.org>  Tue, 22 Oct 2013 17:56:20 +0200

php-horde-smtp (1.2.3-1) unstable; urgency=low

  * Horde's Horde_Smtp package
  * Initial packaging (Closes: #723574)

 -- Mathieu Parent <sathieu@debian.org>  Tue, 17 Sep 2013 17:16:58 +0200
